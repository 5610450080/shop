package ku.sci.cs.shop;


/** 
 * 
 * น.ส.ชญามณ  กาญจนาพงศาเวช 5610450080 หมู่ 200
 *
 */

import java.util.ArrayList;

public class Inventory {
	private ArrayList<Clothes> AP = new ArrayList<Clothes>();
	 Clothes pro;
	
	public void addProduct(Clothes pro){
		AP.add(pro);
	}
	
	public ArrayList<Clothes> listProduct(){
		return AP;
	}
	
	public String findNo(String no){
		int a = Integer.parseInt(no);
		String s = null;
		for (Clothes p : AP ){
			if (p.getNo() == a){
				s = p.toString();
				break;
			}
			else{
				s = "not found!";
			}
		}
		return s;
	}
	
	public String findName(String name){
		String s = null;
		for (Clothes p : AP ){
			if (p.getName().equals(name)){
				s = p.toString();
				break;
			}
			else{
				s = "not found!";
			}
		}
		return s;
	}
	
	public String findPrice(int price){
		String s = null;
		for (Clothes p : AP ){
			if (p.getPrice() == price){
				s = p.toString();
				break;
			}
			else{
				s = "not found!";
			}
		}
		return s;
	}
	
	public String findColor(String color){
		String s = null;
		for (Clothes p : AP ){
			if (p.getColor().equals(color)){
				s = p.toString();
				break;
			}
			else{
				s = "not found!";
			}
		}
		return s;
	}
	
	public String findBrand(String brand){
		String s = null;
		for (Clothes p : AP ){
			if (p.getBrand().equals(brand)){
				s = p.toString();
				break;
			}
			else{
				s = "not found!";
			}
		}
		return s;
	}
	
	public String findMaterial(String material){
		String s = null;
		for (Clothes p : AP ){
			if (p.getMaterial().equals(material)){
				s = p.toString();
				break;
			}
			else{
				s = "not found!";
			}
		}
		return s;
	}
	
	
	}



