package ku.sci.cs.shop;

/** 
 * 
 * น.ส.ชญามณ  กาญจนาพงศาเวช 5610450080 หมู่ 200
 *
 */

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JRadioButton;

import java.awt.Font;
import java.awt.Panel;

import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ImageIcon;
import javax.swing.JTabbedPane;
import javax.swing.JToggleButton;

import java.awt.ScrollPane;
import java.awt.Scrollbar;

import javax.swing.JScrollPane;
import javax.swing.JLayeredPane;
import javax.swing.JSplitPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.Color;

import javax.swing.JTextArea;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;


public class createGUI {

	private JFrame frmKkk;
	private JTextField textNo;
	private JTextField textField_1;
	private Clothes cs;
	private Inventory in = new Inventory();
	private JTextField textName;
	private JTextField textPrice;
	private JTextField textColor;
	private JTextField textBrand;
	private JTextField textMaterial;
	private JTextArea textArea;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					createGUI window = new createGUI();
					window.frmKkk.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public createGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmKkk = new JFrame();
		frmKkk.setTitle("Poloy's Store");
		frmKkk.setBounds(100, 100, 513, 603);
		frmKkk.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmKkk.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(163, 240, 39, -28);
		frmKkk.getContentPane().add(scrollPane);
		
		JPanel panelClothes = new JPanel();
		panelClothes.setBorder(new TitledBorder(null, "Clothes", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelClothes.setBounds(10, 11, 477, 169);
		frmKkk.getContentPane().add(panelClothes);
		panelClothes.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBounds(55, 21, 100, 100);
		lblNewLabel_1.setIcon(new ImageIcon("C:\\Users\\Administrator\\Desktop\\SE\\sweater1.png"));
		panelClothes.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setBounds(190, 21, 100, 95);
		lblNewLabel_2.setIcon(new ImageIcon("C:\\Users\\Administrator\\Desktop\\SE\\t-shirt1.png"));
		panelClothes.add(lblNewLabel_2);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(316, 12, 120, 120);
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Administrator\\Desktop\\SE\\Dress-Shirt1.png"));
		panelClothes.add(lblNewLabel);
		
		final JRadioButton button_shirt = new JRadioButton("Shirt");
		button_shirt.setBounds(350, 139, 53, 23);
		panelClothes.add(button_shirt);
		button_shirt.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		final JRadioButton button_sweat = new JRadioButton("Sweater");
		button_sweat.setBounds(65, 139, 73, 23);
		panelClothes.add(button_sweat);
		button_sweat.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		final JRadioButton button_Tshirt = new JRadioButton("TShirt");
		button_Tshirt.setBounds(213, 139, 59, 23);
		panelClothes.add(button_Tshirt);
		button_Tshirt.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Inventory", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 191, 477, 121);
		frmKkk.getContentPane().add(panel);
		panel.setLayout(null);
		
		textNo = new JTextField();
		textNo.setBounds(39, 22, 68, 20);
		panel.add(textNo);
		textNo.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("No :");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_3.setBounds(10, 25, 46, 14);
		panel.add(lblNewLabel_3);
		
		JLabel lblName = new JLabel("Name :");
		lblName.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblName.setBounds(118, 25, 46, 14);
		panel.add(lblName);
		
		textName = new JTextField();
		textName.setColumns(10);
		textName.setBounds(164, 22, 100, 20);
		panel.add(textName);
		
		JLabel lblPrice = new JLabel("Price :");
		lblPrice.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPrice.setBounds(274, 25, 46, 14);
		panel.add(lblPrice);
		
		textPrice = new JTextField();
		textPrice.setColumns(10);
		textPrice.setBounds(315, 22, 68, 20);
		panel.add(textPrice);
		
		JLabel lblBaht = new JLabel("Baht.");
		lblBaht.setBounds(393, 25, 46, 14);
		panel.add(lblBaht);
		
		JLabel lblColor = new JLabel("Color :");
		lblColor.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblColor.setBounds(10, 53, 46, 14);
		panel.add(lblColor);
		
		textColor = new JTextField();
		textColor.setColumns(10);
		textColor.setBounds(49, 50, 76, 20);
		panel.add(textColor);
		
		JLabel lblBrand = new JLabel("Brand :");
		lblBrand.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblBrand.setBounds(141, 53, 52, 14);
		panel.add(lblBrand);
		
		textBrand = new JTextField();
		textBrand.setColumns(10);
		textBrand.setBounds(188, 50, 92, 20);
		panel.add(textBrand);
		
		JLabel lblMaterial = new JLabel("Material :");
		lblMaterial.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblMaterial.setBounds(294, 53, 68, 14);
		panel.add(lblMaterial);
		
		textMaterial = new JTextField();
		textMaterial.setColumns(10);
		textMaterial.setBounds(354, 53, 85, 20);
		panel.add(textMaterial);
		
		JButton btnSave = new JButton("Add in Inventory");
		btnSave.setBounds(140, 78, 180, 23);
		panel.add(btnSave);
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (button_shirt.isSelected()){
					Shirt sh = new Shirt(getTextName(), getTextNo(), getTextPrice(), getTextColor(), getTextBrand());
					in.addProduct(sh);
					textArea.append("\n"+sh.toString());
				}
				else if (button_sweat.isSelected()){
					Sweater sw = new Sweater(getTextName(), getTextNo(), getTextPrice(), getTextColor(), getTextBrand(), getTextMaterial());
					in.addProduct(sw);
					textArea.append("\n"+sw.toString());
				}
				else if (button_Tshirt.isSelected()){
					TShirt ts = new TShirt(getTextName(), getTextNo(), getTextPrice(), getTextColor(), getTextBrand());
					in.addProduct(ts);
					textArea.append("\n"+ts.toString());
				}
			}
		});
		btnSave.setForeground(Color.RED);
		btnSave.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "SearchBy", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(10, 323, 477, 63);
		frmKkk.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		final JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		comboBox_1.setBounds(67, 21, 63, 20);
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"No", "Name", "Price", "Color", "Brand", "Material"}));
		panel_1.add(comboBox_1);
		
		textField_1 = new JTextField();
		textField_1.setBounds(140, 21, 200, 20);
		panel_1.add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnSearch = new JButton("SEARCH");
		btnSearch.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnSearch.setBounds(350, 20, 89, 23);
		panel_1.add(btnSearch);
		btnSearch.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String item = String.valueOf(comboBox_1.getSelectedItem());
				if (item == "No"){
					textArea.setText(in.findNo(getTextField_1()));
				}
				else if (item == "Name"){
					textArea.setText(in.findName(getTextField_1()));
				}
				else if (item == "Price"){
					int i = Integer.parseInt(getTextField_1());
					textArea.setText(in.findPrice(i));
				}
				else if (item == "Color"){
					textArea.setText(in.findColor(getTextField_1()));
				}
				else if (item == "Brand"){
					textArea.setText(in.findBrand(getTextField_1()));
				}
				else if (item == "Material"){
					textArea.setText(in.findMaterial(getTextField_1()));
				}
				
			}
		});
		
		textArea = new JTextArea();
		textArea.setBounds(10, 397, 477, 152);
		frmKkk.getContentPane().add(textArea);
	}
	
	public int getTextNo(){
		return Integer.parseInt(textNo.getText());
	}
	
	public String getTextName(){
		return textName.getText();
	}
	
	public int getTextPrice(){
		return Integer.parseInt(textPrice.getText());
	}
	
	public String getTextColor(){
		return textColor.getText();
	}
	
	public String getTextBrand(){
		return textBrand.getText();
	}
	
	public String getTextMaterial(){
		return textMaterial.getText();
	}
	
	public String getTextField_1(){
		return textField_1.getText();
	}
	
}
