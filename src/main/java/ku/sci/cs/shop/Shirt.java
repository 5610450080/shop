package ku.sci.cs.shop;

/** 
 * 
 * น.ส.ชญามณ  กาญจนาพงศาเวช 5610450080 หมู่ 200
 *
 */

public class Shirt extends Clothes{

	public Shirt(String name, int no, int price, String color, String brand) {
		super(name, no, price, color, brand);
		// TODO Auto-generated constructor stub
	}
	
	public String getColor() {
		return super.getColor();
	}


	public void setColor(String color) {
		super.setColor(color);
	}



	public String getBrand() {
		return super.getBrand();
	}



	public void setBrand(String brand) {
		super.setBrand(brand);
	}

	public void setName(String name) {
		super.setName(name);
	}

	public String getName(){
		return super.getName();
	}


	public void setNo(int no) {
		super.setNo(no);
	}

	public int getNo(){
		return super.getNo();
	}


	public void setPrice(int price) {
		super.setPrice(price);
	}

	public int getPrice(){
		return super.getPrice();
	}
	
	public String toString(){
		return "No:"+no+" "+name+" "+color+" "+brand+" "+price+" Baht.";
	}

	

}
