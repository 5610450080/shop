package ku.sci.cs.shop;


/** 
 * 
 * น.ส.ชญามณ  กาญจนาพงศาเวช 5610450080 หมู่ 200
 *
 */

public class Clothes {
	protected String name;
	protected int no;
	protected int price;
	protected String color;
	protected String brand;
	protected String material;
	
	public Clothes(String name,int no,int price,String color,String brand){
		this.name = name;
		this.no = no;
		this.price = price;	
		this.color = color;
		this.brand = brand;
		this.material = "";
		
	}
	
	
	public String getColor() {
		return color;
	}



	public void setColor(String color) {
		this.color = color;
	}



	public String getBrand() {
		return brand;
	}



	public void setBrand(String brand) {
		this.brand = brand;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName(){
		return name;
	}


	public void setNo(int no) {
		this.no = no;
	}

	public int getNo(){
		return no;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getPrice(){
		return price;
	}
	
	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	
	public String toString(){
		return "No:"+no+" "+name+" "+color+" "+brand+" "+price+" Baht.";
	}

}

