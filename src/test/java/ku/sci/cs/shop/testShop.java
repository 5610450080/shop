
package ku.sci.cs.shop;

/** 
 * 
 * น.ส.ชญามณ  กาญจนาพงศาเวช 5610450080 หมู่ 200
 *
 */

import static org.junit.Assert.*;


import org.junit.Test;

public class testShop {

	@Test
	public void test() {
		Inventory i = new Inventory();
		Clothes c1 = new Shirt("ABC", 100, 500, "pink", "ZARA");
		Clothes c2 = new TShirt("DEF", 101, 300, "green", "AllZ");
		Clothes c3 = new Sweater("ZZZ", 102, 1020, "blue", "H&M", "wool");
		
		String a1 = "No:100 ABC pink ZARA 500 Baht.";
		String a2 = "No:101 DEF green AllZ 300 Baht.";
		String a3 = "No:102 ZZZ blue H&M wool 1020 Baht.";
		
		i.addProduct(c1);
		i.addProduct(c2);
		i.addProduct(c3);
		
//		assertEquals("No:76 Cat 10 Baht.", i.findNo("76"));
//		assertEquals("No:77 Dog 20 Baht.", i.findNo("77"));
//		assertEquals("No:77 Dog 20 Baht.", i.findName("Dog"));
//		assertEquals("No:76 Cat 10 Baht.", i.findName("Cat"));
//		assertEquals("No:76 Cat 10 Baht.", i.findPrice(10));
//		assertEquals("No:77 Dog 20 Baht.", i.findPrice(20));
		
		assertEquals(a1, i.findBrand("ZARA"));
		assertEquals(a2, i.findColor("green"));
		assertEquals(a3, i.findMaterial("wool"));
		
		assertEquals(a1, i.findName("ABC"));
		assertEquals(a2, i.findNo("101"));
		assertEquals(a3, i.findPrice(1020));
		
		
		
		
	}

	
	

}


	
	

